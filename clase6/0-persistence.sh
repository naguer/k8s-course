k get pv,pvc
k apply -f 0-pv.yaml
k get pv
k apply -f 0-pvc.yaml
k get pvc
k apply -f 0-pod.yaml
k exec -it task-pv-pod -- bash
# echo testingPersistence > /usr/share/nginx/html/index.html
k port-forward task-pv-pod 8080:80
k create -f 0-pod2.yaml
k get pvc
kubectl port-forward task-pv-pod2 8080:80
k delete pod task-pv-pod task-pv-pod2
k apply -f 0-pod3.yaml
k port-forward task-pv-pod3 8080:80
