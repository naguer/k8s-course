k get sts
k apply -f 2-statefulsets.yaml
k get pod -w
kubectl get pod -w -l app=nginx
kubectl delete pod -l app=nginx
kubectl get pod -w -l app=nginx
nslookup web-0.nginx
for i in 0 1; do kubectl exec web-$i -- sh -c 'hostname'; done
kubectl run -i --tty --image busybox:1.28 dns-test --restart=Never --rm /bin/sh
# nslookup web-0.nginx
k get pvc,pv | grep web
for i in 0 1; do kubectl exec "web-$i" -- sh -c 'echo "$(hostname)" > /usr/share/nginx/html/index.html'; done
for i in 0 1; do kubectl exec -i -t "web-$i" -- curl http://localhost/; done
kubectl get pod -w -l app=nginx
kubectl delete pod -l app=nginx
for i in 0 1; do kubectl exec -i -t "web-$i" -- curl http://localhost/; done
kubectl scale sts web --replicas=5
k get pvc,pv
kubectl get pods -w -l app=nginx

