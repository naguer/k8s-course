k get sa
k describe sa default
k create sa jenkins -o yaml --dry-run=client 
k get sa jenkins
k describe sa jenkins
k describe secret jenkins-token-xyz
k run pod jenkins --image nginx -o yaml --dry-run=client # Add serviceAccountName: jenkins
k exec -it jenkins -- bash
# get token
cat /run/secrets/kubernetes.io/serviceaccount/token
