kubectl run nginx --image=nginx -o yaml --dry-run=client > 2-liveness-probe.yaml

kubectl exec -it liveness -- rm /usr/share/nginx/html/index.html
