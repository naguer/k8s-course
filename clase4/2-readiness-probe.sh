kubectl apply -f 2-readiness-probe.yml
kubectl get pods -l app=readiness-deployment
kubectl describe deployment readiness-deployment | grep Replicas:
kubectl exec -it readiness-deployment-548975dcc5-rdmqn -- rm /tmp/healthy
kubectl get pods -l app=readiness-deployment
kubectl describe deployment readiness-deployment | grep Replicas:
# When the readiness probe for a pod fails, the endpoints controller removes the pod from list of endpoints of all services that match the pod.
# Restore service
k exec -it readiness-deployment-75cc58855b-bnp86 -- rm /tmp/healthy
kubectl exec -it readiness-deployment-548975dcc5-rdmqn -- touch /tmp/healthy
kubectl get pods -l app=readiness-deployment

Readineess para que te reinicie?
