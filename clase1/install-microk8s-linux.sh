sudo snap install microk8s --classic
microk8s status --wait-ready
microk8s kubectl get nodes
microk8s kubectl get services
alias kubectl='microk8s kubectl'
microk8s kubectl create deployment nginx --image=nginx
microk8s kubectl get pods
microk8s delete deploy nginx
microk8s stop
microk8s start
