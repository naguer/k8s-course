# Install Docker
sudo yum update -y
#sudo amazon-linux-extras install docker
sudo yum install docker
sudo service docker start
sudo systemctl enable docker
sudo usermod -a -G docker ec2-user

# Create Docker Image
docker run hello-world
touch Dockerfile

# Docker Nginx
docker run -it --rm -d -p 8080:80 --name web nginx

# Dockefile
FROM nginx:latest
RUN echo 'Kubernetes Clase1' > /usr/share/nginx/html/index.html

# Docker build
docker build -t k8s-class1 .
docker images 
docker run -t -i -p 9090:80 k8s-class1
