kubectl apply -f https://github.com/kubernetes-sigs/metrics-server/releases/latest/download/components.yaml
kubectl get deployment metrics-server -n kube-system
# Minishift
# minikube addons enable metrics-server
k top nodes
k top pods
