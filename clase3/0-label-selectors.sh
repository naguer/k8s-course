kubectl create deployment nginx --image=nginx --replicas=1 --labels="env=dev,tier=frontend"
kubectl create deployment nginx --image=nginx --replicas=2 --labels="env=qa,tier=frontend"
kubectl create deployment nginx --image=nginx --replicas=3 --labels="env=prod,tier=frontend"
kubectl get deployments --show-labels
# Equality based selectors. = == !=
kubectl get deployment -l env=dev --show-labels
# Set-based selectors. in notin exists
kubectl get pods -l 'env in (dev,prod)' --show-labels

# Annotations are quite similar to labels only that annotations provide a place to store additional metadata for Kubernetes objects with the main purpose of assisting tools and libraries.
