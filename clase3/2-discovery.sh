kubectl create deploy redis-deploy --image redis --replicas 1
kubectl expose deploy redis-deploy --port=6379 --name redis-service
## Discovery
kubectl exec -it redis-XYZ -- env && cat /etc/resolv.conf
