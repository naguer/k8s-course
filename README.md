# k8s_course

Ejercicios curso Kubernetes separados por clases

Temario:

Clase1:
* Crear EKS Cluster usando EKSCTL
* Conectarse a un Cluster EKS
* Instalar Microk8s en Linux o MacOs

Clase2: 
* Pod
* Namespace
* Replicaset
* Deployment
* Deployment rollouts
