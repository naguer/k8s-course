kubectl get rs
kubectl apply -f 2-replicaset.yml
kubectl describe rs/my-replicaset
kubectl get pods
kubectl delete pod my-replicaset-XXXX
kubectl replace -f 2-replicaset.yml
kubectl scale rs my-replicaset --replicas=5
kubectl get pods my-replicaset-XXXX -o yaml | grep -A 5 owner
kubectl edit pod my-replicaset-XXXX # change label
kubectl delete rs my-replicaset
kubectl delete pod my-replicaset-XXXX
