kubectl create namespace class2
kubectl explain namespace --recursive | less
kubectl config set-context --current --namespace=class2
kubectl config view --minify | grep namespace:
kubectl api-resources --namespaced=true
kubectl api-resources --namespaced=false
