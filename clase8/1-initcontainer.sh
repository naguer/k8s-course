# Create pod with nginx container exposed at port 80. 
# Add a busybox init container which downloads a page using 
# "wget -O /work-dir/index.html http://neverssl.com/online". 
# Make a volume of type emptyDir and mount it in both containers. 
# For the nginx container, mount it on "/usr/share/nginx/html" 
# and for the initcontainer, mount it on "/work-dir". 
# When done, get the IP of the created pod and create a busybox pod and run "wget -O- IP"

kubectl run web --image=nginx --restart=Never --port=80 --dry-run=client -o yaml > pod-init.yaml

kubectl apply -f 1-initcontainer.yml
kubectl get po -o wide
kubectl run box-test --image=busybox --restart=Never -it --rm -- /bin/sh -c "wget -O- IP"

# volume
containers:
  - image: nginx
...
    volumeMounts:
    - name: vol
      mountPath: /usr/share/nginx/html
  volumes:
  - name: vol
    emptyDir: {}

# initContainer
