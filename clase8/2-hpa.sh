kubectl create deployment --image=nginx nginx-deploy --replicas 2 --dry-run=client -o yaml > deployment.yml
kubectl expose deploy nginx-deploy --port=80 --name nginx-service --type=LoadBalancer --dry-run=client -o yaml > expose.yml
kubectl autoscale deployment nginx-deploy --cpu-percent=40 --min=2 --max=6 --dry-run=client -o yaml > hpa.yml
kubectl get hpa
kubectl run -i --tty load-generator --rm --image=busybox --restart=Never -- /bin/sh -c "while sleep 0.01; do wget -q -O- http://nginx-deploy; done"
kubectl get hpa
kubectl get deployment php-apache

   resources:
     requests:
       memory: "16Mi"
       cpu: "25m"