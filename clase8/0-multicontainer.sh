kubectl run busybox --image=busybox --restart=Never -o yaml --dry-run=client -- /bin/sh -c 'echo hello;sleep 3600' > multicontainer.yml

# Copy from args to name, and change to busybox2

kubectl create -f 0-multicontainer.yml
kubectl exec -it busybox -c container2 -- ls

kubectl delete po busybox